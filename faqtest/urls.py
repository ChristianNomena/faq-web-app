from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', include(('faq.urls', 'faq'), namespace='faq')),
]

urlpatterns += i18n_patterns(
    path('api/v1/', include([
        path('auth/', include('djoser.urls')),
        path('auth/', include('djoser.urls.jwt')),
        path('', include('faq.api.v1.urls')),
    ])),

    prefix_default_language=False
)
