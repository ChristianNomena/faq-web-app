# FAQ Web App

## Requirements

- Python 3.9
- Django 4.2
- PostgreSQL 15

## Install

- Create a virtual environment
- ```pip install -r requirements.txt```

## Run

- Create a `.env` file from `.env.example` and configure it if necessary
- Create a database named `faqwebapp` in your PostgreSQL
- ```python manage.py migrate```
- ```python manage.py runserver```

## Features

- Authentication
- Publishing questions
- Replying questions

## Tests

```python manage.py test```

## Endpoints

### Authentication

- `GET` /api/v1/auth/jwt/create/ => Logging and creating jwt tokens [`access : 1 hour`, `refresh : 1 day`]
- `POST` /api/v1/auth/users/ => Creating an account with `username` and `password` 

### FAQ

- `GET` /api/v1/questions/ => Get all the questions [`need to be authenticated`]
- `GET` /api/v1/questions/public => Get all the answered questions
- `GET` /api/v1/questions/waiting => Get all the unanswered questions [`need to be authenticated`]
- `GET` /api/v1/questions/<question_id>/ => Get a specitif question [`need to be authenticated`]
- `PUT` /api/v1/questions/reply/<question_id>/ => Answer a question [`need to be authenticated`]
