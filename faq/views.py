from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from .models import Question


appname = 'faq'


class QuestionListView(ListView):
    model = Question

    def get_queryset(self):
        queryset = Question.objects.exclude(Q(answer__isnull=True) | Q(answer=''))
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class QuestionCreateView(CreateView):
    model = Question
    fields = [
        'question',
        'category',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Veuillez entrer votre question')
        return context

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, _('Votre question a été enregistrée avec succès'))
        return reverse('faq:index')


@login_required(login_url='/login/')
def question_list_admin(request, template=appname+'/question_list_admin.html'):
    object_list = Question.objects.filter(Q(answer__isnull=True) | Q(answer=''))

    context = {
        'object_list': object_list,
    }

    return render(request, template, context)


class UpdateQuestion(LoginRequiredMixin, UpdateView):
    model = Question
    fields = [
        'answer',
    ]
    template_name_suffix = "_update_form"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Veuillez répondre à la question')
        return context

    def get_success_url(self):
        messages.success(self.request, _('Votre réponse a été enregistrée avec succès'))
        return reverse('faq:list_admin')
