from django.db import models
from django.utils.translation import gettext as _


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Question(models.Model):
    category = models.ForeignKey(Category, verbose_name=_("Catégorie"), on_delete=models.SET_NULL, null=True, related_name='questions')
    question = models.TextField(_('Question'))
    answer = models.TextField(_('Réponse'), blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.question

    class Meta:
        ordering = ['-created_at']
