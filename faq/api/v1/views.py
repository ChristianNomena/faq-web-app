from django.db.models import Q
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from faq.models import Question
from .serializers import QuestionSerializer


class QuestionView(APIView):
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method == 'POST':
            return [AllowAny()]
        else:
            return [IsAuthenticated()]

    def get(self, request):
        questions = Question.objects.all()
        serialized_questions = self.serializer_class(questions, many=True)
        return Response(serialized_questions.data)

    def post(self, request):
        serialized_question = self.serializer_class(data=request.data)
        if serialized_question.is_valid():
            serialized_question.save()
            return Response(serialized_question.data, status=status.HTTP_201_CREATED)
        return Response(serialized_question.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionPublicView(APIView):
    serializer_class = QuestionSerializer

    def get(self, request):
        questions = Question.objects.exclude(Q(answer__isnull=True) | Q(answer=''))
        serialized_questions = self.serializer_class(questions, many=True)
        return Response(serialized_questions.data)


class QuestionWaitingView(APIView):
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        questions = Question.objects.filter(Q(answer__isnull=True) | Q(answer=''))
        serialized_questions = self.serializer_class(questions, many=True)
        return Response(serialized_questions.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    serializer_class = QuestionSerializer

    if question:
        return Response(serializer_class(question).data)
    else:
        return Response(question)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def reply_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    serializer_class = QuestionSerializer

    if question:
        if request.data['answer']:
            question.answer = request.data['answer']
            question.save()
            return Response(serializer_class(question).data)
        else:
            return Response({'answer': 'Field required'})
    else:
        return Response(question)
