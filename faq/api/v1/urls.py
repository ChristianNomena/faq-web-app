from django.urls import path
from . import views

urlpatterns = [
    path('questions/', views.QuestionView.as_view(), name='questions'),
    path('questions/public/', views.QuestionPublicView.as_view(), name='questions_public'),
    path('questions/waiting/', views.QuestionWaitingView.as_view(), name='questions_waiting'),
    path('questions/<int:pk>', views.get_question, name='question_get'),
    path('questions/reply/<int:pk>', views.reply_question, name='question_reply'),
]
