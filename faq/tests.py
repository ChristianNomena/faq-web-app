from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from faq.api.v1.serializers import QuestionSerializer
from faq.models import Question, Category


appname = 'faq'


class CategoryModelTestCase(TestCase):
    def test_category_creation(self):
        category = Category.objects.create(name='Test Category')
        self.assertEqual(category.name, 'Test Category')
        self.assertIsNotNone(category.created_at)


class QuestionModelTestCase(TestCase):
    def setUp(self):
        self.category = Category.objects.create(name='Test Category')
        self.question_data = {
            'category': self.category,
            'question': 'Test Question',
            'answer': 'Test Answer',
        }
        self.question = Question.objects.create(**self.question_data)

    def test_question_creation(self):
        question = Question.objects.create(
            category=self.category,
            question='Test Question',
            answer='Test Answer'
        )
        self.assertEqual(question.category, self.category)
        self.assertEqual(question.question, 'Test Question')
        self.assertEqual(question.answer, 'Test Answer')
        self.assertIsNotNone(question.created_at)
        self.assertIsNotNone(question.updated_at)

    def test_question_update(self):
        updated_question_data = {
            'question': 'Updated Question',
            'answer': 'Updated Answer',
        }
        self.question.question = updated_question_data['question']
        self.question.answer = updated_question_data['answer']
        self.question.save()

        updated_question = Question.objects.get(pk=self.question.pk)
        self.assertEqual(updated_question.question, updated_question_data['question'])
        self.assertEqual(updated_question.answer, updated_question_data['answer'])

    def test_question_delete(self):
        question_id = self.question.pk
        self.question.delete()

        with self.assertRaises(Question.DoesNotExist):
            deleted_question = Question.objects.get(pk=question_id)

    def test_question_select(self):
        selected_question = Question.objects.get(pk=self.question.pk)
        self.assertEqual(selected_question.question, self.question_data['question'])
        self.assertEqual(selected_question.answer, self.question_data['answer'])
        self.assertEqual(selected_question.category, self.category)


class QuestionListViewTestCase(TestCase):
    def test_question_list_view(self):
        response = self.client.get(reverse('faq:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, appname+'/question_list.html')


class QuestionCreateViewTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.client.login(username='testuser', password='testpassword')

    def test_question_create_view(self):
        response = self.client.get(reverse('faq:add'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, appname+'/question_form.html')

    def test_question_create_form_submission(self):
        post_data = {
            'question': 'Test Question',
            'category': 'Test Category',
        }
        response = self.client.post(reverse('faq:add'), data=post_data)
        self.assertEqual(response.status_code, 200)


class QuestionListAdminViewTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='admin', password='adminpassword')
        self.client.login(username='admin', password='adminpassword')

    def test_question_list_admin_view(self):
        response = self.client.get(reverse('faq:list_admin'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, appname+'/question_list_admin.html')


class UpdateQuestionViewTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='admin', password='adminpassword')
        self.client.login(username='admin', password='adminpassword')
        self.question = Question.objects.create(question='Test Question')

    def test_update_question_view(self):
        response = self.client.get(reverse('faq:update', args=[self.question.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, appname+'/question_update_form.html')

    def test_update_question_form_submission(self):
        post_data = {
            'answer': 'Test Answer',
        }
        response = self.client.post(reverse('faq:update', args=[self.question.pk]), data=post_data)
        self.assertEqual(response.status_code, 302)
