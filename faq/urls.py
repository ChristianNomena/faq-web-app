from django.urls import path
from . import views


urlpatterns = [
    path('', views.QuestionListView.as_view(), name='index'),
    path('add/', views.QuestionCreateView.as_view(), name='add'),
    path('list/', views.question_list_admin, name='list_admin'),
    path('update/<int:pk>/', views.UpdateQuestion.as_view(), name='update'),
]
