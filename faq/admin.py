from django.contrib import admin
from .models import Question, Category


class QuestionAdmin(admin.ModelAdmin):
    list_display = [
        'question',
        'answer',
        'category',
    ]


class CategoryAdmin(admin.ModelAdmin):
    list_display = [
        'name',
    ]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Category, CategoryAdmin)
